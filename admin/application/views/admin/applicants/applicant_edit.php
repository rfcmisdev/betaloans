<?php 
  # initializes s3 connection 
  require_once __DIR__.'/initialize.php'; 
?>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; View Applicants</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/applicants'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Applicant List</a>
        </div>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>

    <div class="col-md-6">
      <ul id="user-details" class="list-group">
        <div class="list-group-item bg-dark-blue text-light" contenteditable="false"><strong>Applicant Information</strong></div>
        <? if ($user['firstname']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>First Name</strong></span> <?= $user['firstname']; ?></li>
        <? } ?>
        <? if ($user['middlename']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Middle Name</strong></span> <?= $user['middlename']; ?></li>
        <? } ?>
        <? if ($user['lastname']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Last Name</strong></span> <?= $user['lastname']; ?></li>
        <? } ?>
        <? if ($user['suffix']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Suffix</strong></span> <?= $user['suffix']; ?></li>
        <? } ?>
        <? if ($user['email']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Email</strong></span> <?= $user['email']; ?></li>
        <? } ?>
        <? if ($user['mobile_no']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Mobile Number</strong></span> <?= $user['mobile_no']; ?></li>
        <? } ?>
        <? if ($user['marital_status']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Marital Status</strong></span> <?= $user['marital_status']; ?></li>
        <? } ?>
        <? if ($user['gender']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Gender</strong></span> <?= $user['gender']; ?></li> 
        <? } ?>
        <? if ($user['birthday']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Birthday</strong></span> <?= $user['birthday']; ?></li>  
        <? } ?>
        <? if ($user['age']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Age</strong></span> <?= $user['age']; ?></li>
        <? } ?>
        <? if ($user['educational_attainment']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Educational Attainment</strong></span> <?= $user['educational_attainment']; ?></li> 
        <? } ?>
        <? if ($user['nationality']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Nationality</strong></span> <?= $user['nationality']; ?></li>  
        <? } ?>
        <? if ($user['residency']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Residency</strong></span> <?= $user['residency']; ?></li> 
        <? } ?>
        <? if ($user['province']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Province</strong></span> <?= $user['province']; ?></li>
        <? } ?>
        <? if ($user['city']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>City</strong></span> <?= $user['city']; ?></li> 
        <? } ?>
        <? if ($user['barangay']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Barangay</strong></span> <?= $user['barangay']; ?></li>
        <? } ?>
        <? if ($user['street_address']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Street Address</strong></span> <?= $user['street_address']; ?></li> 
        <? } ?>
        <? if ($user['years_of_stay']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Years of Stay</strong></span> <?= $user['years_of_stay']; ?></li>
        <? } ?>
        <? if ($user['months_of_stay']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Months of Stay</strong></span> <?= $user['months_of_stay']; ?></li>
        <? } ?>
        <? if ($user['landline']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Landline</strong></span> <?= $user['landline']; ?></li> 
        <? } ?>
        <? if ($user['number_of_dependents']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Number of Dependents</strong></span> <?= $user['number_of_dependents']; ?></li> 
        <? } ?>
        <? if ($user['spouse_first_name']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse First Name</strong></span> <?= $user['spouse_first_name']; ?></li>  
        <? } ?>
        <? if ($user['spouse_middle_name']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Middle Name</strong></span> <?= $user['spouse_middle_name']; ?></li> 
        <? } ?>
        <? if ($user['spouse_last_name']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Last Name</strong></span> <?= $user['spouse_last_name']; ?></li> 	
        <? } ?>
        <? if ($user['spouse_civil_status']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Civil Status</strong></span> <?= $user['spouse_civil_status']; ?></li> 
        <? } ?>
        <? if ($user['spouse_gender']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Gender</strong></span> <?= $user['spouse_gender']; ?></li> 
        <? } ?>
        <? if ($user['spouse_birthday']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Birthday</strong></span> <?= $user['spouse_birthday']; ?></li> 
        <? } ?>
        <? if ($user['spouse_educational_attainment']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Educational Attainment</strong></span> <?= $user['spouse_educational_attainment']; ?></li> 
        <? } ?>
        <? if ($user['spouse_employment_type']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Employment Type</strong></span> <?= $user['spouse_employment_type']; ?></li> 
        <? } ?>
        <? if ($user['spouse_employer_name']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Spouse Employer Name</strong></span> <?= $user['spouse_employer_name']; ?></li> 
        <? } ?>
        <? if ($user['employment_nature']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Nature of Employment</strong></span> <?= $user['employment_nature']; ?></li> 
        <? } ?>
        <? if ($user['employer_name']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Employer Name</strong></span> <?= $user['employer_name']; ?></li> 
        <? } ?>
        <? if ($user['sector']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Sector</strong></span> <?= $user['sector']; ?></li> 
        <? } ?>
        <? if ($user['position']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Position</strong></span> <?= $user['position']; ?></li> 
        <? } ?>
        <? if ($user['profession']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Profession</strong></span> <?= $user['profession']; ?></li>
        <? } ?>
        <? if ($user['employment_years']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Employment Years</strong></span> <?= $user['employment_years']; ?></li>
        <? } ?>
        <? if ($user['employment_months']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Employment Months</strong></span> <?= $user['employment_months']; ?></li>
        <? } ?>
        <? if ($user['monthly_salary']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Monthly Salary</strong></span> <?= $user['monthly_salary']; ?></li>
        <? } ?>
        <? if ($user['non_core_business']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Non-Core Business</strong></span> <?= $user['non_core_business']; ?></li> 
        <? } ?>
        <? if ($user['business_name']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Business Name</strong></span> <?= $user['business_name']; ?></li> 
        <? } ?>
        <? if ($user['business_type']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Business Type</strong></span> <?= $user['business_type']; ?></li> 
        <? } ?>
        <? if ($user['industry']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Industry</strong></span> <?= $user['industry']; ?></li> 
        <? } ?>
        <? if ($user['business_years']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Business Years</strong></span> <?= $user['business_years']; ?></li> 
        <? } ?>
        <? if ($user['business_months']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Business Months</strong></span> <?= $user['business_months']; ?></li>
        <? } ?>
        <? if ($user['monthly_income']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Monthly Income</strong></span> <?= $user['monthly_income']; ?></li>
        <? } ?>
        <? if ($user['loan_purpose']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Loan Purpose</strong></span> <?= $user['loan_purpose']; ?></li> 
        <? } ?>
        <? if ($user['other_loan_purpose']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Other Loan Purpose</strong></span> <?= $user['other_loan_purpose']; ?></li>
        <? } ?>
        <? if ($user['loan_product']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Loan Product</strong></span> <?= $user['loan_product']; ?></li>
        <? } ?>
        <? if ($user['sub_product']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Sub Product</strong></span> <?= $user['sub_product']; ?></li>
        <? } ?>
        <? if ($user['tenure']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Tenure</strong></span> <?= $user['tenure']; ?></li> 
        <? } ?>
        <? if ($user['repayment_frequency']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Repayment Frequency</strong></span> <?= $user['repayment_frequency']; ?></li>
        <? } ?>
        <? if ($user['loan_amount_requested']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Loan Amount Requested</strong></span> <?= $user['loan_amount_requested']; ?></li> 
        <? } ?>
        <? if ($user['source_of_information']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Source of Information</strong></span> <?= $user['source_of_information']; ?></li>
        <? } ?>
        <? if ($user['additional_income']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Additional Income</strong></span> <?= $user['additional_income']; ?></li>
        <? } ?>
        <? if ($user['living_expense']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Living Expense</strong></span> <?= $user['living_expense']; ?></li>
        <? } ?>

        <? if ($user['monthly_salary']) { ?>       
          <li class="list-group-item text-right"><span class="pull-left"><strong>Gross Income</strong></span> <?= $user['gross_income']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Net Disposable Income</strong></span> <?= $user['net_disposable_income']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Net Disposable Income with Buffer</strong></span> <?= $user['net_disposable_income_buffer']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Total Expenses</strong></span> <?= $user['total_expense']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Excess / Deficiency</strong></span> <?= $user['excess_deficiency']; ?></li>
        <? } else if ($user['monthly_income']) { ?>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Gross Income</strong></span> <?= $user['gross_income']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Net Disposable Income</strong></span> <?= $user['net_disposable_income']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Net Disposable Income with Buffer</strong></span> <?= $user['net_disposable_income_buffer']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Total Expenses</strong></span> <?= $user['total_expense']; ?></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Excess / Deficiency</strong></span> <?= $user['excess_deficiency']; ?></li>
        <? } ?>   

        <? if ($user['collateral_information']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Collateral Information</strong></span> <?= $user['collateral_information']; ?></li>
        <? } ?>
        <? if ($user['collateral_type']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Collateral Type</strong></span> <?= $user['collateral_type']; ?></li>
        <? } ?>
        <? if ($user['collateral_subtype']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Collateral Sub Type</strong></span> <?= $user['collateral_subtype']; ?></li>
        <? } ?>
        <? if ($user['collateral_category']) { ?> 
        <li class="list-group-item text-right"><span class="pull-left"><strong>Collateral Category</strong></span> <?= $user['collateral_category']; ?></li>
        <? } ?>
        <? if ($user['average_fair_market_value']) { ?>
        <li class="list-group-item text-right"><span class="pull-left"><strong>Average Fair Market Value</strong></span> <?= $user['average_fair_market_value']; ?></li>
        <? } ?>
      
<?
try{ 
	# initializing our object 
	$files = $s3->getIterator('ListObjects', array( # this is a Generator Object (its yields data rather than returning) 
	'Bucket' => $config['s3-access']['bucket'],
	'Prefix' =>  $user['key_name']
  )); 
  
  $x = 0;

	# printing our data 
	foreach($files as $file) { 
	# whether file is private or not 
	$file_acl = $s3->getObjectAcl([ 
	'Bucket' => $config['s3-access']['bucket'], 
	'Key' => $file['Key'] 
	]); 
	$is_private = true; 
	foreach($file_acl['Grants'] as $grant) { 
	if( 
	isset($grant['Grantee']['URI']) && 
	$grant['Grantee']['URI'] == 'http://acs.amazonaws.com/groups/global/AllUsers' && 
	$grant['Permission'] == 'READ' 
	) $is_private = false; # this file is not private 
	} 
	
	# applicable url 
	if($is_private == false){ 
	$file_url = $s3->getObjectUrl($config['s3-access']['bucket'], $file['Key']); 
	}else{ 
	$url_creator = $s3->getCommand('GetObject', [ 
	'Bucket' => $config['s3-access']['bucket'], 
	'Key' => $file['Key'] 
	]); 
	$file_url = $s3->createPresignedRequest($url_creator, '+2 minutes')->getUri(); 
	} 

  
	# printing all 
  echo "<li class='list-group-item text-right'><span class='pull-left'><strong class='file_dl_$x'></strong></span> <a href=". $file_url .">Download</a></li>";

  $x++;
	} 
   }catch(Exception $ex){ 
	echo "Error Occurred\n", $ex->getMessage(); 
   } 
?>       

      </ul>  
    </div>

    <div class="col-md-3"></div>
  </div>  

</section>

<style>
.modal-body > img{
  width: 100% !important;
}
</style>

<script>
$( ".file_dl_0" ).text( "Primary Valid ID" );
$( ".file_dl_1" ).text( "Secondary Valid ID" );
$( ".file_dl_2" ).text( "Latest Billing" );
$( ".file_dl_3" ).text( "Latest Payslip" );
$( ".file_dl_4" ).text( "Employment Verification" );
$( ".file_dl_5" ).text( "Certificate of Employment" );
$( ".file_dl_6" ).text( "DTI / Mayors Permit" );
</script>