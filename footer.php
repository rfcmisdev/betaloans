<footer class="l-footer" itemscope="" itemtype="https://schema.org/Footer">
    <section class="l-section wpb_row height_large color_footer-bottom ult-vc-hide-row vc_row-has-fill" data-rtl="false" data-row-effect-mobile-disable="true" style="position: relative;">
        <div class="upb_row_bg" data-bg-override="0" style="background: rgb(30, 30, 30); min-width: 1903px; left: 0px; width: 1903px;"></div>
        <div class="l-section-h i-cf">
            <div class="g-cols row type_default valign_middle">
                <div class="col-sm-12 wpb_column vc_column_container">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="w-socials align_center style_outlined hover_fade color_link shape_circle">
                                <div class="w-socials-list" style="font-size:20px;margin:-10px;">
                                    <div class="w-socials-item facebook" style="margin:10px;">
                                        <a class="w-socials-item-link" target="_blank" href="https://www.facebook.com/EASYRFC/" aria-label="Facebook" rel="nofollow"><span class="w-socials-item-link-hover"></span></a>
                                        <div class="w-socials-item-popup"><span>Facebook</span></div>
                                    </div>
                                    <div class="w-socials-item twitter" style="margin:10px;">
                                        <a class="w-socials-item-link" target="_blank" href="https://twitter.com/EASYRFC" aria-label="Twitter" rel="nofollow"><span class="w-socials-item-link-hover"></span></a>
                                        <div class="w-socials-item-popup"><span>Twitter</span></div>
                                    </div>
                                    <div class="w-socials-item instagram" style="margin:10px;">
                                        <a class="w-socials-item-link" target="_blank" href="https://www.instagram.com/easyrfc/" aria-label="Instagram" rel="nofollow"><span class="w-socials-item-link-hover"></span></a>
                                        <div class="w-socials-item-popup"><span>Instagram</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-separator type_invisible size_medium"></div>
                            <div class="g-cols wpb_row type_default valign_top vc_inner ">
                                <div class="col-sm-12 wpb_column vc_column_container">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column  align_center">
                                                <div class="wpb_wrapper">
                                                    <p>Copyright © <span class="highlight_secondary">Radiowealth Finance Company</span>, All Rights Reserved.</p>
                                                    <p><a title="Terms and Condition" href="https://www.rfc.com.ph/terms" target="_blank">Terms of Use</a>&nbsp; &nbsp;|&nbsp; &nbsp; <a title="Privacy Policy" href="https://www.rfc.com.ph/privacy" target="_blank">Privacy Policy</a>&nbsp; &nbsp;|&nbsp; &nbsp; <a title="Sitemap" href="https://www.rfc.com.ph/sitemap" target="_blank">Sitemap</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>   
</footer>
<style>
    .l-footer {
        color: #4e4e4e;
        background-color: #ffffff;
        overflow: hidden;
        margin: 0 auto;
        width: 100%;        
    }
    .l-footer .l-section {
        padding-left: 2.5rem;
        padding-right: 2.5rem;
    }
    .color_footer-bottom {
        color: #6c6c6c;
    }   
    .color_footer-bottom, .color_footer-bottom *, 
    .color_footer-bottom .w-btn.color_light {
        border-color: #424242;
    }
    .color_footer-bottom {
        background-color: #2c2c2c;
    }
    .l-section:before {
        content: '';
        display: table;
    }         
    .upb_row_bg {
        position: absolute;
        width: 100%;
        height: 100%;
        background-position: center center;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        z-index: 0;
        overflow: hidden;
    }    
    .l-section.height_large>.l-section-h {
        padding: 6rem 0;
    }    
    .g-cols.type_default {
        margin: 0 -1.5rem;
    }    
    .g-cols {
        display: flex;
        flex-wrap: wrap;
    }
    .g-cols>div {
        display: flex;
        flex-direction: column;
    }
    .wpb_column {
        position: relative;        
    }
    .g-cols.valign_middle>div>.vc_column-inner {
        justify-content: center;
    }
    .w-socials-item.twitter .w-socials-item-link-hover {
    background-color: #55acee;
}    .g-cols.type_default>div>.vc_column-inner {
        padding-left: 1.5rem;
        padding-right: 1.5rem;
    }       
    .g-cols>div>.vc_column-inner {
        display: flex;
        flex-direction: column;
        flex-grow: 1;
        flex-shrink: 0;
    }
    .color_footer-bottom, .color_footer-bottom *, .color_footer-bottom .w-btn.color_light {
        border-color: #424242;
    }     
    .w-socials-item {
        display: inline-block;
        vertical-align: top;
        position: relative;
    }    
    .color_footer-bottom .w-socials.style_outlined .w-socials-item-link {
        box-shadow: 0 0 0 2px #424242 inset;
    }    
    .w-socials.shape_circle .w-socials-item-link {
        border-radius: 50%;
    }
    .color_footer-bottom a {
        color: #9fa3a7;
    }
    .w-socials-item-link {
        display: block;
        text-align: center;
        position: relative;
        overflow: hidden;
        line-height: 2.5em;
        width: 2.5em;
        border: none !important;
        border-radius: inherit;
        z-index: 0;
    }
    .no-touch .w-socials.hover_fade .w-socials-item-link-hover {
        display: block;
        opacity: 0;
        transition: opacity 0.3s;
    }    
    .w-socials-item.facebook .w-socials-item-link-hover {
        background-color: #42599e;
    }        
    .w-socials-item.facebook .w-socials-item-link:after {
        content: '\f39e';
    }   
    .w-socials-item-link:after {
        font-family: 'Font Awesome 5 Brands';
    } 
    .w-socials-item-link:after, .w-socials-item-link i {
        display: block;
        line-height: inherit;
        position: relative;
    }
    .w-socials-item.twitter .w-socials-item-link:after {
        content: '\f099';
    }
    .w-socials-item.instagram .w-socials-item-link-hover {
        background: #a17357;
        background: radial-gradient(circle farthest-corner at 35% 100%, #fec564, transparent 50%),radial-gradient(circle farthest-corner at 10% 140%, #feda7e, transparent 50%),radial-gradient(ellipse farthest-corner at 0 -25%, #5258cf, transparent 50%),radial-gradient(ellipse farthest-corner at 20% -50%, #5258cf, transparent 50%),radial-gradient(ellipse farthest-corner at 100% 0, #893dc2, transparent 50%),radial-gradient(ellipse farthest-corner at 60% -20%, #893dc2, transparent 50%),radial-gradient(ellipse farthest-corner at 100% 100%, #d9317a, transparent),linear-gradient(#6559ca, #bc318f 30%, #e42e66 50%, #fa5332 70%, #ffdc80 100%);
    }   
    .w-socials-item.instagram .w-socials-item-link:after {
        content: '\f16d';
        font-size: 1.1em;
    }     
    .w-separator.type_invisible.size_medium {
        margin: 1.5rem 0;
    }
    .w-separator.type_invisible {
        height: 1px;
    }    
    .w-separator {
        clear: both;
        overflow: hidden;
        line-height: 1.2rem;
        height: 1.2rem;
    }    
    .highlight_secondary {
        color: #ffc80a;
    }
    .color_footer-bottom a {
        color: #9fa3a7;
    }   
    .rounded_none .w-socials-item-popup {
        border-radius: 0 !important;
    }     
    .no-touch .w-socials-item-popup {
        display: block;
    }  
    .w-socials-item-popup {
        display: none;
        position: absolute;
        left: 50%;
        bottom: 100%;
        text-align: center;
        white-space: nowrap;
        z-index: 90;
        font-size: 0.9rem;
        line-height: 2.4rem;
        padding: 0 1rem;
        margin-bottom: 7px;
        border-radius: 0.25rem;
        background-color: rgba(0,0,0,0.8);
        color: #fff;
        opacity: 0;
        visibility: hidden;
        transform: translate3d(-50%,-1em,0);
        transition: opacity 0.2s cubic-bezier(.78,.13,.15,.86) 0.3s, transform 0.2s cubic-bezier(.78,.13,.15,.86) 0.3s;
    }
    .w-socials-item-popup:after {
        content: '';
        display: block;
        position: absolute;
        left: 50%;
        bottom: -7px;
        margin-left: -8px;
        width: 0;
        height: 0;
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        border-top: 8px solid rgba(0,0,0,0.8);
    }      
    @font-face{
        font-family:'Font Awesome 5 Brands';
        font-style:normal;
        font-weight:normal;
        src:url("./assets/fonts/fa-brands-400.woff2") format("woff2"),url("./assets/fonts/fa-brands-400.woff") format("woff")
    }
    .fab{
        font-family:'Font Awesome 5 Brands'
    }
    @font-face{
        font-family:'fontawesome';
        font-style:normal;
        font-weight:300;
        src:url("./assets/fonts/fa-light-300.woff2") format("woff2"),url("./assets/fonts/fa-light-300.woff") format("woff")
    }
    .fal{
        font-family:'fontawesome';
        font-weight:300
    }
    @font-face{
        font-family:'fontawesome';
        font-style:normal;
        font-weight:400;
        src:url("./assets/fonts/fa-regular-400.woff2") format("woff2"),url("./assets/fonts/fa-regular-400.woff") format("woff")
    }
    .far{
        font-family:'fontawesome';
        font-weight:400
    }
    @font-face{
        font-family:'fontawesome';
        font-style:normal;
        font-weight:900;
        src:url("./assets/fonts/fa-solid-900.woff2") format("woff2"),url("./assets/fonts/fa-solid-900.woff") format("woff")
    }
</style>