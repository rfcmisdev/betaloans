-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 19, 2018 at 06:23 AM
-- Server version: 5.7.24-0ubuntu0.16.04.1-log
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rfcapp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_registered`
--

CREATE TABLE `ci_registered` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ci_registered`
--

INSERT INTO `ci_registered` (`id`, `email`, `validation_id`, `created_at`) VALUES
(1, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(2, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(3, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(4, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(5, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(6, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(7, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(8, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(9, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(10, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(11, 'john.go@hsgsoftware.com', '472a152.44028443', '2018-10-31'),
(12, 'go.john91@gmail.com', '89feee8.53197814', '2018-11-08');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `suffix` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `middlename`, `lastname`, `suffix`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'Admin', 'admin', '', 'admin', '', 'admin@webiato.co', '12345', '$2y$10$VCT944aeaqq/jrTAIZkrL.fsOiDEZW5y0oBFQhbAoEw.kDHiamj.O', 1, 1, 1, '', '2017-09-29 10:09:44', '2018-10-04 01:10:04'),
(62, 'jjmarimat', 'JHayar', '', 'Marimat', '', 'rfc.mis.jjmarimat@rfc.com.ph', '09286986592', '$2y$10$Kea867ayPBUGAlyquBm5p.UWcGGfzgHUNeudpS1wvOeFYf9K3T/j2', 1, 1, 0, '', '2018-10-23 02:10:19', '2018-10-23 02:10:19'),
(61, 'rrsalas', 'Ruchell', '', 'Salas', '', 'rrsalas@rfc.com.ph', '123456', '$2y$10$7jD/q2BGX.3yylKhec9a9uXKmtq.tM.rifaWUmUijz1PguAZau/pS', 1, 1, 0, '', '2018-10-09 06:10:36', '2018-10-09 06:10:36');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users_applicant`
--

CREATE TABLE `ci_users_applicant` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `suffix` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `educational_attainment` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `residency` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `barangay` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `years_of_stay` varchar(255) NOT NULL,
  `months_of_stay` varchar(255) NOT NULL,
  `landline` varchar(255) NOT NULL,
  `number_of_dependents` varchar(255) NOT NULL,
  `spouse_first_name` varchar(255) NOT NULL,
  `spouse_middle_name` varchar(255) NOT NULL,
  `spouse_last_name` varchar(255) NOT NULL,
  `spouse_civil_status` varchar(255) NOT NULL,
  `spouse_gender` varchar(255) NOT NULL,
  `spouse_birthday` varchar(255) NOT NULL,
  `spouse_educational_attainment` varchar(255) NOT NULL,
  `spouse_employment_type` varchar(255) NOT NULL,
  `spouse_employer_name` varchar(255) NOT NULL,
  `employment_nature` varchar(255) NOT NULL,
  `employer_name` varchar(255) NOT NULL,
  `sector` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `employment_years` varchar(255) NOT NULL,
  `employment_months` varchar(255) NOT NULL,
  `monthly_salary` varchar(255) NOT NULL,
  `non_core_business` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `industry` varchar(255) NOT NULL,
  `business_years` varchar(255) NOT NULL,
  `business_months` varchar(255) NOT NULL,
  `monthly_income` varchar(255) NOT NULL,
  `loan_purpose` varchar(255) NOT NULL,
  `other_loan_purpose` varchar(255) NOT NULL,
  `loan_product` varchar(255) NOT NULL,
  `sub_product` varchar(255) NOT NULL,
  `tenure` varchar(255) NOT NULL,
  `repayment_frequency` varchar(255) NOT NULL,
  `loan_amount_requested` varchar(255) NOT NULL,
  `source_of_information` varchar(255) NOT NULL,
  `additional_income` varchar(255) NOT NULL,
  `living_expense` varchar(255) NOT NULL,
  `collateral_information` varchar(255) NOT NULL,
  `collateral_type` varchar(255) NOT NULL,
  `collateral_subtype` varchar(255) NOT NULL,
  `collateral_category` varchar(255) NOT NULL,
  `average_fair_market_value` varchar(255) NOT NULL,
  `gross_income` varchar(255) NOT NULL,
  `net_disposable_income` varchar(255) NOT NULL,
  `net_disposable_income_buffer` varchar(255) NOT NULL,
  `total_expense` varchar(255) NOT NULL,
  `excess_deficiency` varchar(255) NOT NULL,
  `key_name` varchar(255) NOT NULL,
  `file_1` varchar(255) NOT NULL,
  `file_2` varchar(255) NOT NULL,
  `file_3` varchar(255) NOT NULL,
  `file_4` varchar(255) NOT NULL,
  `file_5` varchar(255) NOT NULL,
  `file_6` varchar(255) NOT NULL,
  `file_7` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users_applicant`
--

INSERT INTO `ci_users_applicant` (`id`, `username`, `firstname`, `middlename`, `lastname`, `suffix`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`, `marital_status`, `gender`, `birthday`, `age`, `educational_attainment`, `nationality`, `residency`, `province`, `city`, `barangay`, `street_address`, `years_of_stay`, `months_of_stay`, `landline`, `number_of_dependents`, `spouse_first_name`, `spouse_middle_name`, `spouse_last_name`, `spouse_civil_status`, `spouse_gender`, `spouse_birthday`, `spouse_educational_attainment`, `spouse_employment_type`, `spouse_employer_name`, `employment_nature`, `employer_name`, `sector`, `position`, `profession`, `employment_years`, `employment_months`, `monthly_salary`, `non_core_business`, `business_name`, `business_type`, `industry`, `business_years`, `business_months`, `monthly_income`, `loan_purpose`, `other_loan_purpose`, `loan_product`, `sub_product`, `tenure`, `repayment_frequency`, `loan_amount_requested`, `source_of_information`, `additional_income`, `living_expense`, `collateral_information`, `collateral_type`, `collateral_subtype`, `collateral_category`, `average_fair_market_value`, `gross_income`, `net_disposable_income`, `net_disposable_income_buffer`, `total_expense`, `excess_deficiency`, `key_name`, `file_1`, `file_2`, `file_3`, `file_4`, `file_5`, `file_6`, `file_7`) VALUES
(93, '38109301', 'John', 'Calda', 'Go', '', 'go.john91@gmail.com', '+639776647174', '&#zrdNMw!EWT', 7, 0, 0, '120.29.115.55', '2018-11-19 05:32:09', '2018-11-19 05:32:09', 'Single', 'Male', '01-September-1991', '27', 'College Graduate', 'Filipino', 'Resident', 'METRO MANILA', 'MUNTINLUPA CITY', 'SUCAT', '218', '10', '4', '', '0', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', '', 'N/A', 'N/A', 'N/A', 'employed', 'Goliath', 'Computer Software', 'Staff', '', '11', '5', '45000', '', '', '', '', '', '', '0', 'Personal', '', 'Multi-purpose Loan', 'Professional Loan', '45', 'Monthly', '100000', 'Internet', '0', '20000', 'non-collateral', 'No Collateral', 'No Collateral', 'No Collateral', '0', '45000', '25000', '12500', '20000', '20000', 'John_Go-38109301/', 'https://s3.ap-southeast-1.amazonaws.com/newsite-external/John_Go-38109301/01-primary_id-Go_John.jpg', 'https://s3.ap-southeast-1.amazonaws.com/newsite-external/John_Go-38109301/02-secondary_id-Go_John.jpg', 'https://s3.ap-southeast-1.amazonaws.com/newsite-external/John_Go-38109301/03-billing-Go_John.jpg', 'https://s3.ap-southeast-1.amazonaws.com/newsite-external/John_Go-38109301/04-payslip-Go_John.jpg', 'https://s3.ap-southeast-1.amazonaws.com/newsite-external/John_Go-38109301/05-signed_consent-Go_John.jpg', 'https://s3.ap-southeast-1.amazonaws.com/newsite-external/John_Go-38109301/06-certificate_employment-Go_John.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_groups`
--

CREATE TABLE `ci_user_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_user_groups`
--

INSERT INTO `ci_user_groups` (`id`, `group_name`) VALUES
(1, 'management'),
(2, 'support');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_registered`
--
ALTER TABLE `ci_registered`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_users_applicant`
--
ALTER TABLE `ci_users_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_registered`
--
ALTER TABLE `ci_registered`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `ci_users_applicant`
--
ALTER TABLE `ci_users_applicant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
